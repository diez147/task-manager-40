package ru.tsc.babeshko.tm.exception.entity;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}